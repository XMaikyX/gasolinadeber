﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlexanderPenafiel3C
{
    class Venta
    {
        double Total;
        double Subtotal;
        double Adicional;
        double Iva = 0.12;
        int opcion;

        public void CalcularGasolina()
        {
            Gasolina gasolina = new Gasolina();
            Console.WriteLine("Elija que tipo de gasolina desea usar, Elija un numero \n 1.- Extra \n 2.- Super  ");
            opcion = Convert.ToInt32(Console.ReadLine());
            if (opcion == 1)
            {
                Console.WriteLine("Usted ha elegido Extra");
                Console.WriteLine("\n Elija la cantidad de Galones\n");
                gasolina.TipoGasolina = Convert.ToInt32(Console.ReadLine());
                Subtotal = gasolina.TipoGasolina * gasolina.Galon;
                Adicional = Subtotal * Iva;
                Total = Adicional + Subtotal;

            }
            else
            {
                if(opcion == 2)
                {
                    Console.WriteLine("Usted ha elegido Super");
                    Console.WriteLine("\n Elija la cantidad de Galones");
                    gasolina.TipoGasolina = Convert.ToInt32(Console.ReadLine());
                    Subtotal = gasolina.TipoGasolina * gasolina.Galon1;
                    Adicional = Subtotal * Iva;
                    Total = Adicional + Subtotal;
                }

            }   
        }
        public void imprimir()
        {
            Console.WriteLine("Subtotal: " + Subtotal);
            Console.WriteLine("Valor adicional: " + Adicional);
            Console.WriteLine("Valor Final: " + Total);

        }
    }
}
