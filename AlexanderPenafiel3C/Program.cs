﻿using System;

namespace AlexanderPenafiel3C
{
    class Program
    {
        static void Main(string[] args)
        {
           
            Console.WriteLine("         Bienvenido a La gasolinera \n Ingrese los siguientes datos para continuar");
            Cliente cliente = new Cliente();
            Console.WriteLine("Ingrese sus nombres");
            cliente.nombre = Console.ReadLine();
            Console.WriteLine("Ingrese sus Apellidos");
            cliente.apellido = Console.ReadLine();
            Console.WriteLine("Ingrese su Numero de Cedula");
            cliente.cedula = Console.ReadLine();
            Console.WriteLine("Ingrese su Direccion de Vivienda");
            cliente.direccion = Console.ReadLine();


            Console.WriteLine("\n   PROCESO DE COMPRA \n");
            Venta venta = new Venta();
            venta.CalcularGasolina();
            Console.WriteLine("\n     DATOS DEL CONSUMIDOR");
            Console.WriteLine("Nombres:  " + cliente.nombre);
            Console.WriteLine("Apellidos:  " + cliente.apellido);
            Console.WriteLine("Numero de Cedula:  " + cliente.cedula);
            Console.WriteLine("Direccion:  " + cliente.direccion + "\n");
            venta.imprimir();
        }
    }
}
